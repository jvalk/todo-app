export interface ITask {
  id: number
  title: string | null
  completed: boolean
}
